package ru.unn.ega.rucksack;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class DantzigAlgorithm extends RucksackSolver {
    @Override
    public List<Boolean> solveTask(List<Integer> costs, List<Integer> weights, int weightLimit) {
        int totalWeight = 0;
        List<Double> priority = new ArrayList<>();
        for(int i = 0; i<costs.size(); i++) {
            priority.add(costs.get(i).doubleValue() / weights.get(i).doubleValue());
        }
        System.out.println("Priorities generated: "  +priority);
        List<Integer> order = IntStream.iterate(0, a->a+1).limit(weights.size()).boxed()
                .sorted(Comparator.comparingDouble(priority::get).reversed())
                .collect(Collectors.toList());
        System.out.println("Order generated: " + order);
        Boolean[] taken = new Boolean[weights.size()];
        Arrays.fill(taken, false);
        for (int i = 0; i< order.size(); i++) {
            int next = order.get(i);
            if (totalWeight + weights.get(next) > weightLimit) break;
            taken[next] = true;
            totalWeight += weights.get(next);
            System.out.println("Step "+i+"; Chosen: " + (next+1) + "; Weight: " + weights.get(next)
                            + "; Cost: " + costs.get(i) + "; Total weight: " + totalWeight);
        }
        return Arrays.asList(taken);
    }
}
