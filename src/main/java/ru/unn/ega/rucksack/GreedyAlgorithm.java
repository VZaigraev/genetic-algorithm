package ru.unn.ega.rucksack;

import java.util.Arrays;
import java.util.List;

public class GreedyAlgorithm extends RucksackSolver {
    @Override
    public List<Boolean> solveTask(List<Integer> costs, List<Integer> weights, int weightLimit) {
        int totalWeight = 0;
        Boolean[] taken = new Boolean[weights.size()];
        Arrays.fill(taken, false);
        for(int k = 0;;k++) {
            int maxIndex = -1;
            int max = -1;
            for(int i = 0; i < costs.size(); i++) {
                if(!taken[i] && costs.get(i)>max) {
                    maxIndex = i;
                    max = costs.get(i);
                }
            }
            if(maxIndex==-1 || totalWeight+weights.get(maxIndex) > weightLimit) break;
            taken[maxIndex] = true;
            totalWeight += weights.get(maxIndex);
            System.out.println("Step " + k + "; Chosen: " + (maxIndex+1) + "; Cost:" + max + "; Weight:" + weights.get(maxIndex)
                    + "; Total weight: " + totalWeight);
        }
        return Arrays.asList(taken);
    }
}
