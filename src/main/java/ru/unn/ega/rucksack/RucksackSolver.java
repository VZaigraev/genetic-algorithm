package ru.unn.ega.rucksack;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public abstract class RucksackSolver {
    public abstract List<Boolean> solveTask(List<Integer> costs, List<Integer> weights, int weightLimit);

    public List<Boolean> solveFromFile(File file) {
        List<Integer> costs = new ArrayList<>();
        List<Integer> weights = new ArrayList<>();
        int weightLimit = 0;
        try(Scanner scanner = new Scanner(file)) {
           while(scanner.hasNextLine()) {
               String[] line = scanner.nextLine().split(" ");
               if(line.length==1) {
                   weightLimit = Integer.valueOf(line[0]);
               } else if(line.length==3) {
                   costs.add(Integer.valueOf(line[1]));
                   weights.add(Integer.valueOf(line[2]));
               }
           }
        } catch (FileNotFoundException e) {
            System.out.println("File reading exception");
            return null;
        }
        System.out.println("Task loaded:");
        for (int i = 0; i < costs.size(); i++) {
            System.out.println((i+1) + " cost: " + costs.get(i) + "; weight: " + weights.get(i) + ";");
        }
        System.out.println("Weight limit: "+weightLimit);
        List<Boolean> result = solveTask(costs, weights, weightLimit);
        System.out.println("Task solved, result:");
        int totalCost = 0;
        int totalWeight = 0;
        for (int i = 0; i < result.size(); i++) {
            if(result.get(i)) {
                System.out.printf("%d ", i + 1);
                totalCost += costs.get(i);
                totalWeight += weights.get(i);
            }
        }
        System.out.println("\nTotal cost: "+ totalCost);
        System.out.println("Total weight: " + totalWeight);
        return result;
    }
}
