package ru.unn.ega.algorythms;

import ru.unn.ega.datamodel.ResultTuple;

import java.util.Random;
import java.util.Stack;
import java.util.function.Function;

public class HCWidthAlgorythm extends AbstractSolver {

    @Override
    protected ResultTuple solveTask(int depth, int length, Function<Integer,String> codingFunction,
                                    Function<String, Integer> decodingFunction,
                                    Function<Integer, Integer> fitnessFunction) {
        int upperBound = (int) Math.pow(2,length);
        Random random = new Random();
        int val = random.nextInt(upperBound);
        String maxS = codingFunction.apply(val);
        int max = fitnessFunction.apply(val);
        Stack<String> omega = generateLocal(maxS);
        for (int i=0; i < depth; i++) {
            System.out.println("Step:" + i + ", max="
                    + max +", maxS=\""+ maxS +"\" \nOmega: ");
            String maxOmega = "";
            int maxOmegaVal = Integer.MIN_VALUE;
            for (String s : omega) {
                int fitness = fitnessFunction.apply(decodingFunction.apply(s));
                if(fitness>maxOmegaVal) {
                    maxOmega = s;
                    maxOmegaVal = fitness;
                }
                System.out.printf(" s:" + s + "; u:" + fitnessFunction.apply(decodingFunction.apply(s)));
            }
            System.out.println("\nChosen s: " + maxOmega + "; u:" + maxOmegaVal + (maxOmegaVal>max?" MaxS changed":""));
            if(maxOmegaVal > max) {
                max = maxOmegaVal;
                maxS = maxOmega;
                omega = generateLocal(maxOmega);
            } else break;
        }
        System.out.println("Task solved, best value: " + max + ", best coding: " + maxS + '\n');
        return new ResultTuple(max, maxS);
    }

    private Stack<String> generateLocal(String val) {
        Stack<String> stack = new Stack<>();
        char[] data = val.toCharArray();
        for (int i = 0; i < val.length(); i++) {
            char c = data[i];
            data[i] = c == '0' ? '1' : '0';
            stack.add(new String(data));
            data[i] = c;
        }
        return stack;
    }
}
