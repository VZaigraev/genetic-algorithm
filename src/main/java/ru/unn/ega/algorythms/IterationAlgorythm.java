package ru.unn.ega.algorythms;

import ru.unn.ega.datamodel.ResultTuple;

import java.util.Random;
import java.util.function.Function;

public class IterationAlgorythm extends AbstractSolver {

    @Override
    protected ResultTuple solveTask(int depth, int length, Function<Integer, String> codingFunction,
                                    Function<String, Integer> decodingFunction,
                                    Function<Integer, Integer> fitnessFunction) {
        Random random = new Random();
        String names[] = {"Monte-Carlo", "Depth-first", "Width-first"};
        AbstractSolver solvers[] = {new MonteCarloAlgorythm(), new HCDepthAlgorythm(), new HCWidthAlgorythm()};

        int max = 0;
        String maxS = "";
        for (int i = 0; i < depth; i++) {
            int k = random.nextInt(3);
            System.out.println("Step " + i + "; max=" + max + ", maxS=" + maxS + " Started " + names[k] + " search");
            AbstractSolver solver = solvers[k];
            ResultTuple resultTuple = solver.solveTask(depth, length, codingFunction, decodingFunction, fitnessFunction);
            System.out.println("Search finished, results:" + resultTuple);
            if (resultTuple.getMax() > max) {
                max = resultTuple.getMax();
                maxS = resultTuple.getMaxS();
                System.out.println("Maximum changed");
            }
        }
        System.out.println("Task solved, best value: " + max + ", best coding: " + maxS + '\n');
        return new ResultTuple(max, maxS);
    }
}
