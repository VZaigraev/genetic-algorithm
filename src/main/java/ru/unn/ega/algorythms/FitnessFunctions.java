package ru.unn.ega.algorythms;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.function.Function;

public enum FitnessFunctions {
    RANDOM_DISTRIBUTION(length -> new Function<Integer, Integer>() {
        private Random random = new Random(13);
        private Map<Integer, Integer> prev = new HashMap<>();
        public Integer apply(Integer i) {
            if(!prev.containsKey(i)) {
                int val = random.nextInt((int) Math.pow(2, length));
                prev.put(i,val);
                return val;
            } else return prev.get(i);
        }
    }),
    NATURAL_VALUE(length -> i -> i),
    FUNCTION(length -> i -> (int)Math.pow(i - Math.pow(2,length-1), 2)),
    SINUS_LOG_FUNCTION(length -> i ->
            new Double(5*Math.sin(i+128) +
                    Math.log(i+128)).intValue());
    private Function<Integer, Function<Integer,Integer>> function = null;

    private FitnessFunctions(Function<Integer, Function<Integer,Integer>> function) {
        this.function = function;
    }

    public Function<Integer, Function<Integer,Integer>> getFunction() {
        return function;
    }
}
