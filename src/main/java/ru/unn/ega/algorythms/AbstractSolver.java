package ru.unn.ega.algorythms;


import ru.unn.ega.datamodel.ResultTuple;

import java.util.function.Function;

public abstract class AbstractSolver {
    public ResultTuple solveTask(int depth, int length, CodingFunctions codingFunction, FitnessFunctions fitnessFunction) {
        Function<Integer, String> code = codingFunction.encode().apply(length);
        Function<String, Integer> decode = codingFunction.decode().apply(length);
        Function<Integer, Integer> fitness = fitnessFunction.getFunction().apply(length);
        if(fitnessFunction.equals(FitnessFunctions.RANDOM_DISTRIBUTION)) {
            for (int i = 0; i < Math.pow(2, length); i++) {
                fitness.apply(i);
            }
        }
        return solveTask(depth, length, code, decode, fitness);
    }

    protected abstract ResultTuple solveTask(int depth, int length, Function<Integer, String> codingFunction,
                                             Function<String, Integer> decodingFunction,
                                             Function<Integer, Integer> fitnessFunction);
}
