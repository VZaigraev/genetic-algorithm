package ru.unn.ega.algorythms;

import org.omg.PortableInterceptor.INACTIVE;

import java.util.Arrays;
import java.util.function.Function;

public enum CodingFunctions {
    BINARY_CODING((depth)->(value)->{
        String nextS = Integer.toBinaryString(value);
        char[] start = new char[depth-nextS.length()];
        Arrays.fill(start,'0');
        nextS = new String(start) + nextS;
        return nextS;
    }, (length)->(s)-> Integer.parseInt(s,2)),
    GRAY_CODING((length)->(value)->{
        int grayValue = value ^ (value >> 1);
        String nextS = Integer.toBinaryString(grayValue);
        char[] start = new char[length-nextS.length()];
        Arrays.fill(start,'0');
        nextS = new String(start) + nextS;
        return nextS;
    }, (length)->(s)->{
        int grayValue = Integer.valueOf(s,2);
        int value = 0;
        for( ; grayValue>0; grayValue>>=1) {
            value ^= grayValue;
        }
        return value;
    });
    private Function<Integer, Function<Integer, String>> encoding;
    private Function<Integer, Function<String, Integer>> decoding;

    CodingFunctions(Function<Integer, Function<Integer,String>> encoding,
                    Function<Integer, Function<String, Integer>> decoding){
        this.encoding = encoding;
        this.decoding = decoding;
    }

    public Function<Integer, Function<Integer, String>> encode() {
        return encoding;
    }

    public Function<Integer, Function<String, Integer>> decode() {
        return decoding;
    }
}
