package ru.unn.ega.algorythms;

import ru.unn.ega.datamodel.ResultTuple;

import java.util.*;
import java.util.function.Function;

public class HCDepthAlgorythm extends AbstractSolver {

    @Override
    protected ResultTuple solveTask(int depth, int length, Function<Integer,String> codingFunction,
                                    Function<String, Integer> decodingFunction,
                                    Function<Integer, Integer> fitnessFunction) {
        int upperBound = (int) Math.pow(2,length);
        Random random = new Random();
        int val = random.nextInt(upperBound);
        String maxS = codingFunction.apply(val);
        int max = fitnessFunction.apply(val);
        Stack<String> omega = generateLocal(maxS);
        for (int i=0; i < depth; i++) {
            System.out.println("Step:" + i + ", max="
                    + max +", maxS=\""+ maxS +"\" \nOmega: ");
            for (String s : omega) {
                System.out.printf(" s:" + s + "; u:" + fitnessFunction.apply(decodingFunction.apply(s)));
            }
            if(!omega.isEmpty()) {
                int index = random.nextInt(omega.size());
                String newS = omega.get(index);
                omega.remove(index);
                int fitness = fitnessFunction.apply(decodingFunction.apply(newS));
                System.out.println("\nChosen s: " + newS + "; u:" + fitness + (fitness>max?" MaxS changed":""));
                if(fitness > max) {
                    max = fitness;
                    maxS = newS;
                    omega = generateLocal(newS);
                }
            } else break;
        }
        System.out.println("Task solved, best value: " + max + ", best coding: " + maxS + '\n');
        return new ResultTuple(max, maxS);
    }

    private Stack<String> generateLocal(String val) {
        Stack<String> stack = new Stack<>();
        char[] data = val.toCharArray();
        for (int i = 0; i < val.length(); i++) {
            char c = data[i];
            data[i] = c == '0' ? '1' : '0';
            stack.add(new String(data));
            data[i] = c;
        }
        return stack;
    }
}
