package ru.unn.ega.algorythms;

import ru.unn.ega.datamodel.ResultTuple;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;
import java.util.function.Function;

public class MonteCarloAlgorythm extends AbstractSolver {

    protected ResultTuple solveTask(int depth, int length, Function<Integer, String> codingFunction,
                                    Function<String, Integer> decodingFunction,
                                    Function<Integer, Integer> fitnessFunction) {
        Set<Integer> used = new HashSet<>();
        int upperBound = (int) Math.pow(2,length);
        Random random = new Random();
        int max = Integer.MIN_VALUE;
        String maxS = "";
        for(int i = 0; i< depth; i++) {
            int next = 0;
            do {
                next = random.nextInt(upperBound);
            } while(used.contains(next));
            used.add(next);
            String nextS = codingFunction.apply(next);
            int fitness = fitnessFunction.apply(next);
            System.out.println("Step:" + i + ", max="
                    + max +", maxS=\""+ maxS +"\" " +"New s:\"" + nextS + "\" u(s)="+fitness +
                    (fitness>max ? " Maximum changed to new value":""));
            if(max < fitness) {
                max = fitness;
                maxS = nextS;
            }
        }
        System.out.println("Task solved, best value: " + max + ", best coding: " + maxS + '\n');
        return new ResultTuple(max, maxS);
    }

}
