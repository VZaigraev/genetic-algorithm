package ru.unn.ega.datamodel;

public class ResultTuple {
    private final Integer max;
    private final String maxS;

    public ResultTuple(Integer max, String maxS) {
        this.max = max;
        this.maxS = maxS;
    }

    public Integer getMax() {
        return max;
    }

    public String getMaxS() {
        return maxS;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ResultTuple that = (ResultTuple) o;

        if (max != null ? !max.equals(that.max) : that.max != null) return false;
        return maxS != null ? maxS.equals(that.maxS) : that.maxS == null;
    }

    @Override
    public int hashCode() {
        int result = max != null ? max.hashCode() : 0;
        result = 31 * result + (maxS != null ? maxS.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "max=" + max +
                ", maxS=" + maxS;
    }
}
