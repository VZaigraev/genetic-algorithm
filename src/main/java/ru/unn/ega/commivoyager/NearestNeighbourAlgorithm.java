package ru.unn.ega.commivoyager;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class NearestNeighbourAlgorithm extends CommivoyagerSolver {
    @Override
    public List<Integer> solveTask(List<List<Float>> matrix) {
        List<Integer> variants = Stream.iterate(0,a->a+1).limit(matrix.size()).collect(Collectors.toList());
        List<Integer> result = new ArrayList<>(matrix.size());
        Random random = new Random();
        int peek = random.nextInt(variants.size());
        result.add(peek);
        variants.remove(peek);
        int step = 0;
        System.out.println("Initially chosen city:"+peek);
        while(!variants.isEmpty() && result.size() != matrix.size()) {
            int last = result.get(result.size()-1);
            int next = variants.stream().reduce((a,b) ->
                    matrix.get(last).get(a) < matrix.get(last).get(b) ? a : b).orElse(0);
            result.add(next);
            variants.removeIf(i -> i == next);
            System.out.println("Step "+ step + " Chosen city:" + next +
                    " Distance from previous:"+matrix.get(last).get(next)+"\nCurrent result: "+result);
            step++;
        }
        return result;
    }
}
