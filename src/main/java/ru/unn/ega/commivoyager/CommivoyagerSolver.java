package ru.unn.ega.commivoyager;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

public abstract class CommivoyagerSolver {
    public abstract List<Integer> solveTask(List<List<Float>> matrix);

    public List<Integer> solveFromFile(File file) {
        List<List<Float>> loaded = new ArrayList<>();
        try (Scanner scanner = new Scanner(file)) {
            while(scanner.hasNextLine()) {
                loaded.add(Arrays.stream(scanner.nextLine().split(" "))
                        .map(Float::valueOf).collect(Collectors.toList()));
            }
        } catch (FileNotFoundException e) {
            System.out.println("File reading exception");
            return null;
        }
        int length = loaded.size();
        for (List list : loaded) {
            if(list.size()!=length) {
                System.out.println("Matrix should be square");
                return null;
            }
        }
        System.out.println("Matrix loaded:\n");
        for(int i = 0; i < length; i++) {
            for (int j = 0; j < length; j++)
                System.out.printf("%f, ",loaded.get(i).get(j));
            System.out.println();
        }
        List<Integer> result = solveTask(loaded);
        System.out.println("Task solved, result: "+result);
        float l = 0;
        for (int i = 0; i < result.size()-1; i++)
            l += loaded.get(result.get(i)).get(result.get(i+1));
        l += loaded.get(result.get(result.size()-1)).get(result.get(0));
        System.out.println("Length:"+l);
        return result;
    }
}
