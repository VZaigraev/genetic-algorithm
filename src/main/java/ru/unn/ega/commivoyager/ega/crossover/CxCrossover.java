package ru.unn.ega.commivoyager.ega.crossover;

import ru.unn.ega.commivoyager.ega.Individual;
import ru.unn.ega.commivoyager.ega.fitness.FitnessFunction;
import ru.unn.ega.datamodel.Tuple;

import java.util.*;

public class CxCrossover implements Crossover {
    @Override
    public List<Tuple<Individual, Integer>> crossover(Tuple<Individual, Integer> a, Tuple<Individual, Integer> b, FitnessFunction fitnessFunction) {
        List<Set<Integer>> loops = new ArrayList<>();
        List<Integer> aCode = a.getA().decode();
        List<Integer> bCode = b.getA().decode();
        boolean[] checked = new boolean[aCode.size()];
        Arrays.fill(checked, false);
        for (int i = 0; i < aCode.size() && hasMore(checked);) {
            Set<Integer> loop = new HashSet<>();
            while(checked[i]) i++;
            int j = i;
            while(true) {
                loop.add(j);
                j = aCode.indexOf(bCode.get(j));
                if(loop.contains(j))
                    break;
            }
            for (Integer locus : loop) {
                checked[locus] = true;
            }
            loops.add(loop);
        }
        Integer[] aNext = new Integer[aCode.size()];
        Integer[] bNext = new Integer[aCode.size()];
        for(int i = 0; i < loops.size(); i++) {
            for(Integer locuses : loops.get(i)) {
                if(i%2==0) {
                    aNext[locuses] = aCode.get(locuses);
                    bNext[locuses] = bCode.get(locuses);
                } else {
                    aNext[locuses] = bCode.get(locuses);
                    bNext[locuses] = aCode.get(locuses);
                }
            }
        }
        Individual aIndividual = new Individual(Arrays.asList(aNext));
        Individual bIndividual = new Individual(Arrays.asList(bNext));
        List<Tuple<Individual, Integer>> result = new ArrayList<>();
        result.add(new Tuple<>(aIndividual, fitnessFunction.calculate(aIndividual)));
        result.add(new Tuple<>(bIndividual, fitnessFunction.calculate(bIndividual)));
        return result;
    }

    private boolean hasMore(boolean[] checked) {
        for (boolean b : checked)
            if(!b) return true;
        return false;
    }
}
