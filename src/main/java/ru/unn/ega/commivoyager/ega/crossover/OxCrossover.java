package ru.unn.ega.commivoyager.ega.crossover;

import ru.unn.ega.commivoyager.ega.Individual;
import ru.unn.ega.commivoyager.ega.fitness.FitnessFunction;
import ru.unn.ega.datamodel.Tuple;

import java.util.*;

public class OxCrossover implements Crossover {
    private final Random random = new Random();
    @Override
    public List<Tuple<Individual, Integer>> crossover(Tuple<Individual, Integer> a, Tuple<Individual, Integer> b, FitnessFunction fitnessFunction) {
        List<Tuple<Individual, Integer>> children = new ArrayList<>();
        List<Integer> codeA = a.getA().decode();
        List<Integer> codeB = b.getA().decode();
        int f = random.nextInt(codeA.size()-1);
        int s = random.nextInt(codeA.size()-f-1)+f+1;
        Integer[] fChild = new Integer[codeA.size()];
        Integer[] sChild = new Integer[codeA.size()];
        Arrays.fill(fChild, -1);
        Arrays.fill(sChild, -1);
        for(int i = 0; i < f; i++) {
            fChild[i] = codeA.get(i);
            sChild[i] = codeB.get(i);
        }
        for(int i = s; i < codeA.size(); i++) {
            fChild[i] = codeA.get(i);
            sChild[i] = codeB.get(i);
        }
        fill(fChild, f, s, codeB);
        fill(sChild, f, s, codeA);
        Individual fIndividual = new Individual(Arrays.asList(fChild));
        Individual sIndividual = new Individual(Arrays.asList(sChild));
        children.add(new Tuple<>(fIndividual, fitnessFunction.calculate(fIndividual)));
        children.add(new Tuple<>(sIndividual, fitnessFunction.calculate(sIndividual)));
        return children;
    }

    private void fill(Integer[] array, int f, int s, List<Integer> donor) {
        for(int i = f, j = s; i < s; i++){
            while(contains(array, donor.get(j))) {
                j++;
                if(j==donor.size())
                    j = 0;
            }
            array[i] = donor.get(j);
        }
    }

    private boolean contains(Integer[] array, Integer number) {
        for(Integer i : array)
            if(Objects.equals(number, i))
                return true;
        return false;
    }
}
