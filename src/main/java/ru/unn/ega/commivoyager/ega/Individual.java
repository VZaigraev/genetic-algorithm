package ru.unn.ega.commivoyager.ega;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Individual implements Cloneable {
    private List<Integer> code;

    public Individual(List<Integer> value) {
        code = encode(value);
    }

    public List<Integer> encode(List<Integer> value) {
        List<Integer> row = Stream.iterate(0, a->a+1).limit(value.size()).collect(Collectors.toList());
        List<Integer> code = new ArrayList<>();
        for (Integer aValue : value) {
            code.add(row.indexOf(aValue));
            row.remove((Integer) aValue);
        }
        return code;
    }

    public List<Integer> decode() {
        List<Integer> row = Stream.iterate(0,a->a+1).limit(code.size()).collect(Collectors.toList());
        List<Integer> value = new ArrayList<>();
        for (Integer aValue : code) {
            value.add(row.get(aValue));
            row.remove((int) aValue);
        }
        return value;
    }

    public List<Integer> getCode() {
        return code;
    }

    @Override
    public String toString() {
        return "{" + decode() + '}';
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        List<Integer> clone = new ArrayList<>();
        clone.addAll(decode());
        return new Individual(clone);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Individual that = (Individual) o;

        return code != null ? code.equals(that.code) : that.code == null;
    }

    @Override
    public int hashCode() {
        return code != null ? code.hashCode() : 0;
    }
}
