package ru.unn.ega.commivoyager.ega.fitness;

import ru.unn.ega.commivoyager.ega.Individual;

@FunctionalInterface
public interface FitnessFunction {
    int calculate(Individual individual);
}
