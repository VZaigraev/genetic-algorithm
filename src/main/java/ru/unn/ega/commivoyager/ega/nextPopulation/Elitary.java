package ru.unn.ega.commivoyager.ega.nextPopulation;

import ru.unn.ega.commivoyager.ega.Individual;
import ru.unn.ega.commivoyager.ega.selection.Selection;
import ru.unn.ega.datamodel.Tuple;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class Elitary implements NextPopulationGeneration {
    @Override
    public List<Tuple<Individual, Integer>> calculate(List<Tuple<Individual, Integer>> previous,
                                                      List<Tuple<Individual, Integer>> individuals,
                                                      Selection selection) {
        List<Tuple<Individual, Integer>> result = new ArrayList<>();
        previous.stream().min(Comparator.comparingInt(Tuple::getB)).ifPresent(result::add);
        result.addAll(selection.selection(individuals, previous.size()-1));
        return result;
    }
}
