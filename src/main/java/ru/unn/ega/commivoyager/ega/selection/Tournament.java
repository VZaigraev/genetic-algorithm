package ru.unn.ega.commivoyager.ega.selection;

import ru.unn.ega.commivoyager.ega.Individual;
import ru.unn.ega.datamodel.Tuple;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Tournament implements Selection {
    @Override
    public List<Tuple<Individual, Integer>> selection(List<Tuple<Individual, Integer>> individuals, int populationSize) {
        Random random = new Random();
        List<Tuple<Individual, Integer>> result = new ArrayList<>();
        int pick = 0;
        for(int i = 0; i < populationSize; i++) {
            pick = random.nextInt(individuals.size());
            Tuple<Individual, Integer> a = individuals.get(pick);
            individuals.remove(pick);
            pick = random.nextInt(individuals.size());
            Tuple<Individual, Integer> b = individuals.get(pick);
            individuals.add(a);
            result.add(a.getB() < b.getB() ? a : b);
        }
        return result;
    }
}
