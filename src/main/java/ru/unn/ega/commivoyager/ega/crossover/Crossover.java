package ru.unn.ega.commivoyager.ega.crossover;

import ru.unn.ega.commivoyager.ega.Individual;
import ru.unn.ega.commivoyager.ega.fitness.FitnessFunction;
import ru.unn.ega.datamodel.Tuple;

import java.util.List;

@FunctionalInterface
public interface Crossover {

    List<Tuple<Individual, Integer>> crossover(Tuple<Individual, Integer> a, Tuple<Individual, Integer> b, FitnessFunction fitnessFunction);
}
