package ru.unn.ega.commivoyager.ega.selection;

import ru.unn.ega.commivoyager.ega.Individual;
import ru.unn.ega.commivoyager.ega.RandomObjectSelector;
import ru.unn.ega.datamodel.Tuple;

import java.util.ArrayList;
import java.util.List;

public class Proportional implements Selection {
    @Override
    public List<Tuple<Individual, Integer>> selection(List<Tuple<Individual, Integer>> individuals, int populationSize) {
        RandomObjectSelector<Tuple<Individual, Integer>> selector = new RandomObjectSelector<>();
        int max = individuals.stream().map(Tuple::getB).max(Integer::compareTo).orElse(1000);
        for (Tuple<Individual, Integer> individual : individuals) {
            selector.addObject(individual, max-individual.getB()+1);
        }
        List<Tuple<Individual, Integer>> result = new ArrayList<>();
        for(int i = 0; i < populationSize; i++) {
            result.add(selector.getRandomObject());
        }
        return result;
    }
}
