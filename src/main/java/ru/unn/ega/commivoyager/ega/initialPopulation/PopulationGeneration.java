package ru.unn.ega.commivoyager.ega.initialPopulation;

import ru.unn.ega.commivoyager.ega.Individual;
import ru.unn.ega.commivoyager.ega.fitness.FitnessFunction;
import ru.unn.ega.datamodel.Tuple;

import java.util.List;

@FunctionalInterface
public interface PopulationGeneration {

    List<Tuple<Individual, Integer>> generate(List<List<Float>> matrix, FitnessFunction fitnessFunction, int size);
}
