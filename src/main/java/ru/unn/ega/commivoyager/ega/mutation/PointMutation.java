package ru.unn.ega.commivoyager.ega.mutation;

import ru.unn.ega.commivoyager.ega.Individual;

import java.util.List;
import java.util.Random;

public class PointMutation implements Mutation {
    private final Random random = new Random();
    @Override
    public Individual mutate(Individual individual) {
        List<Integer> code = individual.decode();
        int peek = random.nextInt(code.size()-1);
        Integer buffer = code.get(peek);
        code.set(peek, code.get(peek+1));
        code.set(peek+1, buffer);
        return new Individual(code);
    }
}
