package ru.unn.ega.commivoyager.ega.initialPopulation;

import ru.unn.ega.commivoyager.ega.Individual;
import ru.unn.ega.commivoyager.ega.fitness.FitnessFunction;
import ru.unn.ega.datamodel.Tuple;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class NearestNeighbourGeneration implements PopulationGeneration {
    @Override
    public List<Tuple<Individual, Integer>> generate(List<List<Float>> matrix, FitnessFunction fitnessFunction, int size) {
        Random random = new Random();
        List<Integer> seeds = Stream.iterate(0, a->a+1).limit(matrix.size()).collect(Collectors.toList());
        List<Tuple<Individual, Integer>> individuals = new ArrayList<>();
        for(int s = 0; s < size; s++) {
            List<Integer> variants = Stream.iterate(0,a->a+1).limit(matrix.size()).collect(Collectors.toList());
            List<Integer> result = new ArrayList<>(matrix.size());
            int peek = random.nextInt(seeds.size());
            result.add(seeds.get(peek));
            variants.remove((Integer) seeds.get(peek));
            int step = 0;
//            System.out.println("Initially chosen city:"+peek);
            while(!variants.isEmpty() && result.size() != matrix.size()) {
                int last = result.get(result.size()-1);
                int next = variants.stream().reduce((a,b) ->
                        matrix.get(last).get(a) < matrix.get(last).get(b) ? a : b).orElse(0);
                result.add(next);
                variants.removeIf(i -> i == next);
//                System.out.println("Step "+ step + " Chosen city:" + next +
//                        " Distance from previous:"+matrix.get(last).get(next)+"\nCurrent result: "+result);
                step++;
            }
            Individual individual = new Individual(result);
            individuals.add(new Tuple<>(individual, fitnessFunction.calculate(individual)));
        }
        return individuals;
    }
}
