package ru.unn.ega.commivoyager.ega.initialPopulation;

import ru.unn.ega.commivoyager.ega.Individual;
import ru.unn.ega.commivoyager.ega.fitness.FitnessFunction;
import ru.unn.ega.datamodel.Tuple;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class RandomGeneration implements PopulationGeneration {
    @Override
    public List<Tuple<Individual, Integer>> generate(List<List<Float>> matrix, FitnessFunction fitnessFunction, int size) {
        Random random = new Random();
        List<Tuple<Individual, Integer>> result = new ArrayList<>();
        for(int i = 0; i < size; i++) {
            List<Integer> sequence = Stream.iterate(0, a->a+1)
                    .limit(matrix.size()).collect(Collectors.toList());
            final List<Integer> next = new ArrayList<>();
            next.clear();
            while(!sequence.isEmpty()) {
                int peek = random.nextInt(sequence.size());
                next.add(sequence.get(peek));
                sequence.remove((int)peek);
            }
            Individual individual = new Individual(next);
            result.add(new Tuple<>(individual, fitnessFunction.calculate(individual)));
        }
        return result;
    }
}
