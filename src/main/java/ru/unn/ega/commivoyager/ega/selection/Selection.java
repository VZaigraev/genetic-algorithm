package ru.unn.ega.commivoyager.ega.selection;

import ru.unn.ega.commivoyager.ega.Individual;
import ru.unn.ega.datamodel.Tuple;

import java.util.List;

@FunctionalInterface
public interface Selection {
    List<Tuple<Individual, Integer>> selection(List<Tuple<Individual, Integer>> individuals, int populationSize);
}
