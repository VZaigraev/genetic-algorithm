package ru.unn.ega.commivoyager.ega;

import ru.unn.ega.datamodel.Tuple;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class RandomObjectSelector<T> {
    private List<Tuple<T, Integer>> objects;
    private int bound = 0;
    private final Random random = new Random();

    public RandomObjectSelector(List<Tuple<T, Integer>> objects) {
        this.objects = objects;
        for (Tuple<T, Integer> t : objects) {
            bound += t.getB();
        }
    }

    public RandomObjectSelector() {
        objects = new ArrayList<>();
    }

    public void addObject(T object, int priority) {
        objects.add(new Tuple<>(object, priority));
        bound += priority;
    }

    public T getRandomObject() {
        int peek = random.nextInt(bound);
        for (Tuple<T, Integer> object : objects) {
            peek -= object.getB();
            if (peek <= 0)
                return object.getA();
        }
        return objects.get(0).getA();
    }
}
