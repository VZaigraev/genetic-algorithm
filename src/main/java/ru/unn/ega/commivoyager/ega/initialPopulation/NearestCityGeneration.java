package ru.unn.ega.commivoyager.ega.initialPopulation;

import ru.unn.ega.commivoyager.ega.GeneticAlgorithm;
import ru.unn.ega.commivoyager.ega.Individual;
import ru.unn.ega.commivoyager.ega.fitness.FitnessFunction;
import ru.unn.ega.datamodel.Tuple;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class NearestCityGeneration implements PopulationGeneration {
    @Override
    public List<Tuple<Individual, Integer>> generate(List<List<Float>> matrix, FitnessFunction fitnessFunction, int size) {
        Random random = new Random();
        List<Integer> seeds = Stream.iterate(0, a->a+1).limit(matrix.size()).collect(Collectors.toList());
        List<Tuple<Individual, Integer>> individuals = new ArrayList<>();
        for(int s = 0; s < size; s++) {
            List<Integer> variants = Stream.iterate(0, a -> a + 1)
                    .limit(matrix.size()).collect(Collectors.toList());
            List<Integer> result = new ArrayList<>(matrix.size());
            int peek = random.nextInt(seeds.size());
            result.add(seeds.get(peek));
            variants.remove((Integer) seeds.get(peek));
            int step = 0;
            //System.out.println("Initially chosen city:" + peek);
            while (!variants.isEmpty() && result.size() != matrix.size()) {
                int[] nearest = new int[result.size()];
                float[] distances = new float[result.size()];
                for (Integer i : result) {
                    nearest[result.indexOf(i)] = variants.stream().reduce((a, b) ->
                            matrix.get(i).get(a) < matrix.get(i).get(b) ? a : b).orElse(-1);
                    distances[result.indexOf(i)] = matrix.get(i).get(nearest[result.indexOf(i)]);
                }
                int best = Stream.iterate(0, a -> a + 1).limit(result.size()).filter(i -> nearest[i] != -1)
                        .reduce((a, b) -> distances[a] < distances[b] ? a : b).orElse(-1);
                result.add(best + 1, nearest[best]);
                variants.removeIf(i -> i == nearest[best]);
//                for (int i = 0; i < nearest.length; i++) {
//                    System.out.println(result.get(i) + ": " + nearest[i] + " - " + distances[i]);
//                }
//                System.out.println("Step " + step + " Chosen city:" + nearest[best] +
//                        " Distance from previous:" + distances[best] + "\nCurrent result: " + result);
                step++;
            }
            Individual individual = new Individual(result);
            individuals.add(new Tuple<>(individual, fitnessFunction.calculate(individual)));
        }
        return individuals;
    }
}
