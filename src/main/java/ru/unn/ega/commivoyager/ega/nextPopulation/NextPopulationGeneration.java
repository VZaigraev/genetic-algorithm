package ru.unn.ega.commivoyager.ega.nextPopulation;

import ru.unn.ega.commivoyager.ega.Individual;
import ru.unn.ega.commivoyager.ega.selection.Selection;
import ru.unn.ega.datamodel.Tuple;

import java.util.List;

@FunctionalInterface
public interface NextPopulationGeneration {
    List<Tuple<Individual, Integer>> calculate(List<Tuple<Individual, Integer>> previous,
                                               List<Tuple<Individual, Integer>> individuals,
                                               Selection selection);
}
