package ru.unn.ega.commivoyager.ega.mutation;

import ru.unn.ega.commivoyager.ega.Individual;

@FunctionalInterface
public interface Mutation {
    Individual mutate(Individual individual);
}
