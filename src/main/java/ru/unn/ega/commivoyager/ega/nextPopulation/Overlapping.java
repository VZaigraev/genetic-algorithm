package ru.unn.ega.commivoyager.ega.nextPopulation;

import ru.unn.ega.commivoyager.ega.Individual;
import ru.unn.ega.commivoyager.ega.selection.Selection;
import ru.unn.ega.datamodel.Tuple;

import java.util.List;

public class Overlapping implements NextPopulationGeneration {
    @Override
    public List<Tuple<Individual, Integer>> calculate(List<Tuple<Individual, Integer>> previous, List<Tuple<Individual, Integer>> individuals, Selection selection) {
        int overlapped = (int)(previous.size() * 2. / 3.);
        List<Tuple<Individual, Integer>> next = selection.selection(previous, previous.size()-overlapped);
        next.addAll(selection.selection(individuals, overlapped));
        return next;
    }
}
