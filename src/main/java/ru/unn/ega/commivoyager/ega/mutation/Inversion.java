package ru.unn.ega.commivoyager.ega.mutation;

import ru.unn.ega.commivoyager.ega.Individual;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Inversion implements Mutation {
    private final Random random = new Random();
    @Override
    public Individual mutate(Individual individual) {
        List<Integer> code = individual.decode();
        int a = random.nextInt(code.size()-1);
        int b = a + random.nextInt(code.size()-a) + 1;
        List<Integer> subCode = new ArrayList<>();
        subCode.addAll(code.subList(a,b));
        for(int i = 0; i < b-a; i++) {
            code.set(a+i, subCode.get(subCode.size()-1-i));
        }
        return new Individual(code);
    }
}
