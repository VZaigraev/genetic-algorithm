package ru.unn.ega.commivoyager.ega;

import ru.unn.ega.commivoyager.ega.crossover.Crossover;
import ru.unn.ega.commivoyager.ega.crossover.CxCrossover;
import ru.unn.ega.commivoyager.ega.crossover.OxCrossover;
import ru.unn.ega.commivoyager.ega.initialPopulation.NearestCityGeneration;
import ru.unn.ega.commivoyager.ega.initialPopulation.NearestNeighbourGeneration;
import ru.unn.ega.commivoyager.ega.initialPopulation.PopulationGeneration;
import ru.unn.ega.commivoyager.ega.initialPopulation.RandomGeneration;
import ru.unn.ega.commivoyager.ega.mutation.Inversion;
import ru.unn.ega.commivoyager.ega.mutation.Mutation;
import ru.unn.ega.commivoyager.ega.mutation.PointMutation;
import ru.unn.ega.commivoyager.ega.nextPopulation.Elitary;
import ru.unn.ega.commivoyager.ega.nextPopulation.NextPopulationGeneration;
import ru.unn.ega.commivoyager.ega.nextPopulation.Overlapping;
import ru.unn.ega.commivoyager.ega.selection.Proportional;
import ru.unn.ega.commivoyager.ega.selection.Selection;
import ru.unn.ega.commivoyager.ega.selection.Tournament;
import ru.unn.ega.datamodel.Tuple;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStreamReader;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class GeneticAlgorithm {
    private List<List<Float>> matrix;
    private Random random = new Random();
    private static PopulationGeneration populationGeneration;
    private static Selection selection;
    private static NextPopulationGeneration nextPopulationGeneration;
    private static Crossover crossover;
    private static Mutation mutation;

    public static void main(String[] args) {
        int populationGeneration = 3;
        int crossover = 0;
        int mutation = 0;
        int nextPopulation = 0;
        int selection = 1;
        int populationsCount = 100;
        int populationSize = 50;
        int crossoverPopulationSize = 200;
        double mutationRate = 0.2;
        try (Scanner scanner = new Scanner(new InputStreamReader(System.in)).useLocale(Locale.US)) {
            System.out.println("Starting solving task");
            System.out.println("Use default configuration? y/n");
            String check = scanner.next();
            if(check.contains("n")) {
                System.out.println("Please, input one of the values:\n" +
                        "0 - Nearest City Algorythm\n" +
                        "1 - Nearest Neighbour Algorythm\n" +
                        "2 - Random Algorythm\n" +
                        "3 - Randomly Chosen from previous ones (0.5 for 2, 0.25 for 1, 0.25 for 0)");
                populationGeneration = scanner.nextInt();
                System.out.println("Please, input one of the values:\n" +
                        "0 - CxCrossover\n" +
                        "1 - OxCrossover");
                selection = scanner.nextInt();
                System.out.println("Please, input one of the values:\n" +
                        "0 - Inversion Mutation\n" +
                        "1 - Point Mutation");
                mutation = scanner.nextInt();
                System.out.println("Please, input one of the values:\n" +
                        "0 - Elitary Strategy\n" +
                        "1 - Overlapping Strategy");
                nextPopulation = scanner.nextInt();
                System.out.println("Please, input one of the values:\n" +
                        "0 - Proportional Selection\n" +
                        "1 - Tournament Selection");
                selection = scanner.nextInt();
            }
            System.out.println("Use default parameters? y/n");
            check = scanner.next();
            if(check.contains("n")) {
                System.out.println("Please, input maximum populations count:");
                populationsCount = scanner.nextInt();
                System.out.println("Please, input population size:");
                populationSize = scanner.nextInt();
                System.out.println("Please, input how many pairs should be chosen for crossover:");
                crossoverPopulationSize = scanner.nextInt();
                System.out.println("Please, input mutation rate:");
                mutationRate = scanner.nextDouble();
            }
            System.out.println("Starting using parameters:");
            System.out.println("populationGeneration: " + populationGeneration);
            System.out.println("crossover: "+crossover);
            System.out.println("mutation: "+mutation);
            System.out.println("nextPopulation: "+nextPopulation);
            System.out.println("selection: "+selection);
            System.out.println("populationsCount: "+populationsCount);
            System.out.println("populationSize: " +populationSize);
            System.out.println("crossoverPopulationSize: " + crossoverPopulationSize);
            System.out.println("mutationRate: " + mutationRate);
        }
        switch (populationGeneration) {
            case 0: {
                GeneticAlgorithm.populationGeneration = new NearestCityGeneration();
                break;
            }
            case 1: {
                GeneticAlgorithm.populationGeneration = new NearestNeighbourGeneration();
                break;
            }
            case 2: {
                GeneticAlgorithm.populationGeneration = new RandomGeneration();
                break;
            }
            case 3: {
                RandomObjectSelector<PopulationGeneration> selector = new RandomObjectSelector<>();
                selector.addObject(new NearestNeighbourGeneration(), 1);
                selector.addObject(new NearestCityGeneration(), 1);
                selector.addObject(new RandomGeneration(), 2);
                GeneticAlgorithm.populationGeneration = selector.getRandomObject();
                break;
            }
        }
        switch (selection) {
            case 0: {
                GeneticAlgorithm.selection = new Proportional();
                break;
            }
            case 1: {
                GeneticAlgorithm.selection = new Tournament();
                break;
            }
        }
        switch (nextPopulation) {
            case 0: {
                GeneticAlgorithm.nextPopulationGeneration = new Elitary();
                break;
            }
            case 1: {
                GeneticAlgorithm.nextPopulationGeneration = new Overlapping();
                break;
            }
        }
        switch (crossover) {
            case 0: {
                GeneticAlgorithm.crossover = new CxCrossover();
                break;
            }
            case 1: {
                GeneticAlgorithm.crossover = new OxCrossover();
                break;
            }
        }
        switch (mutation) {
            case 0: {
                GeneticAlgorithm.mutation = new Inversion();
                break;
            }
            case 1: {
                GeneticAlgorithm.mutation = new PointMutation();
                break;
            }
        }
        GeneticAlgorithm.solve(populationsCount, populationSize, crossoverPopulationSize, mutationRate,"./matrices/test.mtx");
    }

    private GeneticAlgorithm(String matrixAddress) {
        this.matrix = loadMatrix(matrixAddress);
    }

    public List<List<Float>> loadMatrix(String path) {
        List<List<Float>> loaded = new ArrayList<>();
        try (Scanner scanner = new Scanner(new File(path))) {
            while(scanner.hasNextLine()) {
                loaded.add(Arrays.stream(scanner.nextLine().split(" "))
                        .map(Float::valueOf).collect(Collectors.toList()));
            }
        } catch (FileNotFoundException e) {
            System.out.println("File reading exception");
            return null;
        }
        int length = loaded.size();
        for (List list : loaded) {
            if(list.size()!=length) {
                System.out.println("Matrix should be square");
                return null;
            }
        }
        System.out.println("Matrix loaded:\n");
        loaded.forEach(aLoaded -> {
            IntStream.range(0, length).forEach(j -> System.out.printf("%f, ", aLoaded.get(j)));
            System.out.println();
        });
        return loaded;
    }

    public static void solve(int maxSteps, int populationSize, int crossoverPopulationSize, double mutationRate, String matrixSize) {
        GeneticAlgorithm geneticAlgorithm = new GeneticAlgorithm(matrixSize);
        geneticAlgorithm.solveTaskPrivate(maxSteps, populationSize, crossoverPopulationSize, mutationRate);
    }

    private void solveTaskPrivate(int maxSteps, int populationSize, int crossoverPopulationSize, double mutationRate) {
        if(matrix == null) return;
        int children = crossoverPopulationSize;
        int mutants = (int)(mutationRate * children);
        List<Tuple<Individual, Integer>> population = populationGeneration
                .generate(matrix, this::getFitness, populationSize);
        List<Tuple<Individual, Integer>> childPopulation = new ArrayList<>();
        List<Tuple<Individual, Integer>> mutantsPopulation = new ArrayList<>();
        System.out.println("Initial population: ");
        System.out.println(population);
        statistics(population);
        for(int i = 0; i < maxSteps; i++) {
            childPopulation.clear();
            mutantsPopulation.clear();
            while(childPopulation.size() < children) {
                int peek = random.nextInt(population.size());
                Tuple<Individual, Integer> a = population.get(peek);
                population.remove(peek);
                peek = random.nextInt(population.size());
                Tuple<Individual, Integer> b = population.get(peek);
                population.add(a);
                childPopulation.addAll(crossover.crossover(a,b, this::getFitness));
            }
            while (mutantsPopulation.size() < mutants) {
                int peek = random.nextInt(population.size());
                Tuple<Individual, Integer> m = population.get(peek);
                Individual mutant = mutation.mutate(m.getA());
                mutantsPopulation.add(new Tuple<>(mutant, getFitness(mutant)));
            }
            List<Tuple<Individual, Integer>> reproductivePopulation = new ArrayList<>();
            reproductivePopulation.addAll(childPopulation);
            reproductivePopulation.addAll(mutantsPopulation);
            population = nextPopulationGeneration.calculate(population, reproductivePopulation,
                    selection);
            System.out.println("Step " + i + ";\n" + population);
            statistics(population);
            int a = population.get(0).getB();
            boolean check = true;
            for(int f = 1; f < populationSize; f++) {
                if(population.get(f).getB()!=a) {
                    check = false;
                    break;
                }
            }
            if(check)
                break;
        }
        System.out.println("\nFinal solution: ");
        statistics(population);
    }

    public int getFitness(Individual individual) {
        List<Integer> permutation = individual.decode();
        int path = 0;
        for(int i = 1; i < permutation.size(); i++) {
            path += matrix.get(permutation.get(i-1)).get(permutation.get(i));
        }
        path += matrix.get(permutation.get(permutation.size()-1)).get(permutation.get(0));
        return path;
    }

    private void statistics(List<Tuple<Individual, Integer>> population) {
        Tuple<Individual, Integer> min = new Tuple<>(null, Integer.MAX_VALUE);
        int max = Integer.MIN_VALUE;
        int sum = 0;
        for (Tuple<Individual, Integer> t : population) {
            if (t.getB() >= max) max = t.getB();
            if (t.getB() <= min.getB()) {
                min = t;
            }
            sum += t.getB();
        }
        System.out.println("Maximum value: " + max + "\nAverage value:" + ((double)sum / population.size()));
        System.out.println("Best value: " + min);
    }

}
