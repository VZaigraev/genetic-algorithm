import org.junit.Test;
import ru.unn.ega.commivoyager.CommivoyagerSolver;
import ru.unn.ega.commivoyager.NearestCityAlgorithm;
import ru.unn.ega.commivoyager.NearestNeighbourAlgorithm;

import java.io.File;

public class CommivoyagerTest {
    @Test
    public void nearestCity1() {
        CommivoyagerSolver solver = new NearestCityAlgorithm();
        solver.solveFromFile(new File("./matrices/matrix.mtx"));
    }

    @Test
    public void nearestCity2() {
        CommivoyagerSolver solver = new NearestCityAlgorithm();
        solver.solveFromFile(new File("./matrices/test.mtx"));
    }

    @Test
    public void nearestNeighbour1() {
        CommivoyagerSolver solver = new NearestNeighbourAlgorithm();
        solver.solveFromFile(new File("./matrices/matrix.mtx"));
    }
    @Test
    public void nearestNeighbour2() {
        CommivoyagerSolver solver = new NearestNeighbourAlgorithm();
        solver.solveFromFile(new File("./matrices/test.mtx"));
    }
}
