import org.junit.Test;
import ru.unn.ega.rucksack.DantzigAlgorithm;
import ru.unn.ega.rucksack.GreedyAlgorithm;
import ru.unn.ega.rucksack.RucksackSolver;

import java.io.File;

public class RucksackTest {
    private RucksackSolver greedy = new GreedyAlgorithm();
    private RucksackSolver dantzig = new DantzigAlgorithm();

    @Test
    public void test1Greedy() {
        greedy.solveFromFile(new File("./rucksack/task1.txt"));
    }

    @Test
    public void test2Greedy() {
        greedy.solveFromFile(new File("./rucksack/task2.txt"));
    }

    @Test
    public void test1Dantzig() {
        dantzig.solveFromFile(new File("./rucksack/task1.txt"));
    }

    @Test
    public void test2Dantzig() {
        dantzig.solveFromFile(new File("./rucksack/task2.txt"));
    }
}
