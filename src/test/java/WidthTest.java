import org.junit.Test;
import ru.unn.ega.algorythms.AbstractSolver;
import ru.unn.ega.algorythms.CodingFunctions;
import ru.unn.ega.algorythms.FitnessFunctions;
import ru.unn.ega.algorythms.HCWidthAlgorythm;

public class WidthTest {
    private AbstractSolver solver = new HCWidthAlgorythm();

    @Test
    public void testRandom() {
        solver.solveTask(3,5, CodingFunctions.BINARY_CODING, FitnessFunctions.RANDOM_DISTRIBUTION);
    }

    @Test
    public void testNatural() {
        solver.solveTask(3,5, CodingFunctions.BINARY_CODING, FitnessFunctions.NATURAL_VALUE);
    }

    @Test
    public void testFunction() {
        solver.solveTask(3,5, CodingFunctions.BINARY_CODING, FitnessFunctions.FUNCTION);
    }
}
