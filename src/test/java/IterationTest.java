import org.junit.Test;
import ru.unn.ega.algorythms.AbstractSolver;
import ru.unn.ega.algorythms.CodingFunctions;
import ru.unn.ega.algorythms.FitnessFunctions;
import ru.unn.ega.algorythms.IterationAlgorythm;

public class IterationTest {
    private AbstractSolver solver = new IterationAlgorythm();

    @Test
    public void test() {
        solver.solveTask(3,7, CodingFunctions.BINARY_CODING, FitnessFunctions.SINUS_LOG_FUNCTION);
    }
}
